package com.chama.test.nearbyplaces.presentation.di

import android.app.Application
import com.chama.test.nearbyplaces.domain.usecase.GetPlacesUseCase
import com.chama.test.nearbyplaces.presentation.viewmodel.PlacesViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
class FactoryModule {

    @Singleton
    @Provides
    fun providePlacesViewModelFactory(
        app: Application,
        getPlacesUseCase: GetPlacesUseCase
    ): PlacesViewModelFactory {
        return PlacesViewModelFactory(app, getPlacesUseCase)
    }

}