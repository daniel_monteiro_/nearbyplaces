package com.chama.test.nearbyplaces.data.repository

import android.util.Log
import com.chama.test.nearbyplaces.data.model.Place
import com.chama.test.nearbyplaces.data.repository.dataSource.PlacesRemoteDataSource
import com.chama.test.nearbyplaces.domain.repository.PlacesRepository
import java.lang.Exception


class PlacesRepositoryImpl(
    private val placesRemoteDataSource: PlacesRemoteDataSource
): PlacesRepository {

    override suspend fun getPlaces(location: String,
                                   radius: String,
                                   type: String): List<Place>? {
        return getPlacesFromAPI(location, radius, type)
    }

    suspend fun getPlacesFromAPI(location: String,
                                 radius: String,
                                 type: String): List<Place> {
        lateinit var placesList: List<Place>

        try {
            val response = placesRemoteDataSource.getPlacesNear(location, radius, type)
            val body = response.body()

            if (body != null) {
                placesList = body.places
            }

        }catch (excpetion: Exception) {
            Log.i("", excpetion.message.toString())
        }

        return placesList
    }
}
