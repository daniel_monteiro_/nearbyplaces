package com.chama.test.nearbyplaces.presentation.cafes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.chama.test.nearbyplaces.MainActivity
import com.chama.test.nearbyplaces.R
import com.chama.test.nearbyplaces.databinding.FragmentCafesBinding
import com.chama.test.nearbyplaces.presentation.adapter.PlacesAdapter
import com.chama.test.nearbyplaces.presentation.adapter.SpacesItemDecoration
import com.chama.test.nearbyplaces.presentation.viewmodel.PlacesViewModel

class CafesFragment : Fragment() {

    private lateinit var binding: FragmentCafesBinding

    private lateinit var placesViewModel: PlacesViewModel
    private lateinit var placesAdapter: PlacesAdapter

    private lateinit var currentLocation: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cafes, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        placesViewModel = (activity as MainActivity).placesViewModel
        placesAdapter = (activity as MainActivity).placesAdapter
        currentLocation = (activity as MainActivity).currentLocationToRequest

        initRecyclerView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            (activity as MainActivity).onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRecyclerView() {
        binding.progressBarCafes.visibility = View.VISIBLE

        val spaceInPixels: Int = resources.getDimensionPixelSize(R.dimen.spacing)
        binding.recyclerViewCafes.layoutManager = LinearLayoutManager(activity)
        binding.recyclerViewCafes.addItemDecoration(SpacesItemDecoration(spaceInPixels))
        placesAdapter = PlacesAdapter()
        binding.recyclerViewCafes.adapter = placesAdapter

        displayPlacesNearCallRequest()
    }

    private fun displayPlacesNearCallRequest() {
        val responseLiveData = placesViewModel.getPlaces(currentLocation, "1000", "cafe")
        responseLiveData.observe((activity as MainActivity), Observer { listPlaces ->
            if (listPlaces != null) {
                binding.progressBarCafes.visibility = View.GONE
                placesAdapter.setList(listPlaces)
                placesAdapter.notifyDataSetChanged()
            }else {
                binding.progressBarCafes.visibility = View.GONE
                Toast.makeText((activity as MainActivity), "No places to show in this distance.", Toast.LENGTH_LONG).show()
            }
        })
    }

}