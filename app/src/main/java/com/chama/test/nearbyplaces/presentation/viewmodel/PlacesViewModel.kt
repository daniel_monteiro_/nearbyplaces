package com.chama.test.nearbyplaces.presentation.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.databinding.Observable
import androidx.lifecycle.*
import com.chama.test.nearbyplaces.data.util.Event
import com.chama.test.nearbyplaces.domain.usecase.GetPlacesUseCase

class PlacesViewModel(
        private val app: Application,
        private val getPlacesNearUseCase: GetPlacesUseCase
): AndroidViewModel(app) {

    fun getPlaces(location: String, radius: String, type: String) = liveData {
        val placesList = getPlacesNearUseCase.execute(location, radius, type)
        placesList?.let { emit(it) }
    }

}