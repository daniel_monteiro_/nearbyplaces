package com.chama.test.nearbyplaces.presentation.di

import com.chama.test.nearbyplaces.data.repository.PlacesRepositoryImpl
import com.chama.test.nearbyplaces.data.repository.dataSource.PlacesRemoteDataSource
import com.chama.test.nearbyplaces.domain.repository.PlacesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun providePlacesRepository(
        placesRemoteDataSource: PlacesRemoteDataSource
    ): PlacesRepository{
        return PlacesRepositoryImpl(placesRemoteDataSource)
    }

}