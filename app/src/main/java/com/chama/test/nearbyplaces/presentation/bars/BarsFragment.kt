package com.chama.test.nearbyplaces.presentation.bars

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.chama.test.nearbyplaces.MainActivity
import com.chama.test.nearbyplaces.R
import com.chama.test.nearbyplaces.databinding.FragmentBarsBinding
import com.chama.test.nearbyplaces.presentation.adapter.PlacesAdapter
import com.chama.test.nearbyplaces.presentation.adapter.SpacesItemDecoration
import com.chama.test.nearbyplaces.presentation.viewmodel.PlacesViewModel

class BarsFragment : Fragment() {

    private lateinit var binding: FragmentBarsBinding

    private lateinit var placesViewModel: PlacesViewModel
    private lateinit var placesAdapter: PlacesAdapter

    private lateinit var currentLocation: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bars, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        placesViewModel = (activity as MainActivity).placesViewModel
        placesAdapter = (activity as MainActivity).placesAdapter
        currentLocation = (activity as MainActivity).currentLocationToRequest

        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.progressBarBars.visibility = View.VISIBLE

        val spaceInPixels: Int = resources.getDimensionPixelSize(R.dimen.spacing)
        binding.recyclerViewBars.layoutManager = LinearLayoutManager(activity)
        binding.recyclerViewBars.addItemDecoration(SpacesItemDecoration(spaceInPixels))
        placesAdapter = PlacesAdapter()
        binding.recyclerViewBars.adapter = placesAdapter

        displayPlacesNearCallRequest()
    }

    private fun displayPlacesNearCallRequest() {
        val responseLiveData = placesViewModel.getPlaces(currentLocation, "1000", "bar")

        responseLiveData.observe((activity as MainActivity), Observer { listPlaces ->
            if (listPlaces != null) {
                binding.progressBarBars.visibility = View.GONE
                placesAdapter.setList(listPlaces)
                placesAdapter.notifyDataSetChanged()
            }else {
                binding.progressBarBars.visibility = View.GONE
                Toast.makeText((activity as MainActivity), "No places to show in this distance.", Toast.LENGTH_LONG).show()
            }
        })
    }
}