package com.chama.test.nearbyplaces.data.repository.dataSourceImpl

import com.chama.test.nearbyplaces.data.api.PlacesAPIService
import com.chama.test.nearbyplaces.data.model.ApiResponse
import com.chama.test.nearbyplaces.data.repository.dataSource.PlacesRemoteDataSource
import retrofit2.Response

class PlacesRemoteDataSourceImpl(
    private val placesAPIService: PlacesAPIService
): PlacesRemoteDataSource {

    override suspend fun getPlacesNear(
        location: String,
        radius: String,
        type: String
    ): Response<ApiResponse> {
        return placesAPIService.getPlaces(location, radius, type)
    }

}
