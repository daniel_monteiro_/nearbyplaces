package com.chama.test.nearbyplaces.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chama.test.nearbyplaces.domain.usecase.GetPlacesUseCase

class PlacesViewModelFactory(
    private val app: Application,
    private val getPlacesUseCase: GetPlacesUseCase
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PlacesViewModel(app, getPlacesUseCase) as T
    }


}