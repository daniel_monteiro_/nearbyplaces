package com.chama.test.nearbyplaces.data.api

import com.chama.test.nearbyplaces.BuildConfig
import com.chama.test.nearbyplaces.data.model.ApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PlacesAPIService {

    @GET("/maps/api/place/nearbysearch/json")
    suspend fun getPlaces(
        @Query("location")
        location: String,
        @Query("radius")
        radius: String,
        @Query("type")
        type: String,
        @Query("key")
        apiKey: String = BuildConfig.GOOGLE_API_KEY
    ): Response<ApiResponse>

}