package com.chama.test.nearbyplaces.presentation.di

import com.chama.test.nearbyplaces.domain.repository.PlacesRepository
import com.chama.test.nearbyplaces.domain.usecase.GetPlacesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetPlaces(placesRepository: PlacesRepository): GetPlacesUseCase {
        return GetPlacesUseCase(placesRepository)
    }

}