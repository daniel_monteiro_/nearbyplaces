package com.chama.test.nearbyplaces.presentation.di

import com.chama.test.nearbyplaces.BuildConfig
import com.chama.test.nearbyplaces.data.api.PlacesAPIService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL_GOOGLE)
            .build()
    }

    @Singleton
    @Provides
    fun providePlacesAPIService(retrofit: Retrofit): PlacesAPIService {
        return retrofit.create(PlacesAPIService::class.java)
    }

}