package com.chama.test.nearbyplaces.domain.usecase

import com.chama.test.nearbyplaces.data.model.Place
import com.chama.test.nearbyplaces.domain.repository.PlacesRepository

class GetPlacesUseCase(private val placesRepository: PlacesRepository) {
    suspend fun execute(location: String,
                        radius: String,
                        type: String): List<Place>? = placesRepository.getPlaces(location, radius, type)
}
