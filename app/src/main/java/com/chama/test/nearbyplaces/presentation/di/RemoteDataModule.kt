package com.chama.test.nearbyplaces.presentation.di

import com.chama.test.nearbyplaces.data.api.PlacesAPIService
import com.chama.test.nearbyplaces.data.repository.dataSource.PlacesRemoteDataSource
import com.chama.test.nearbyplaces.data.repository.dataSourceImpl.PlacesRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class RemoteDataModule {

    @Singleton
    @Provides
    fun providePlacesRemoteDataSource(
        placesAPIService: PlacesAPIService
    ): PlacesRemoteDataSource{
        return PlacesRemoteDataSourceImpl(placesAPIService)
    }

}