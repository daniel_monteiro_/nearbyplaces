package com.chama.test.nearbyplaces.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.chama.test.nearbyplaces.MainActivity
import com.chama.test.nearbyplaces.R
import com.chama.test.nearbyplaces.databinding.FragmentHomeBinding
import com.chama.test.nearbyplaces.presentation.livedata.ConnectionLiveData

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    private var isInternetConnected: Boolean = false

    private lateinit var connectionLiveData: ConnectionLiveData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        binding.apply {

            btnRestaurants.setOnClickListener {
                if (isInternetConnected) {
                    it.findNavController().navigate(R.id.action_homeFragment_to_restaurantFragment)
                }else {
                    Toast.makeText((activity as MainActivity), "No Internet. Connect, please.", Toast.LENGTH_LONG).show()
                }
            }

            btnBars.setOnClickListener {
                if (isInternetConnected) {
                    it.findNavController().navigate(R.id.action_homeFragment_to_barsFragment)
                }else {
                    Toast.makeText((activity as MainActivity), "No Internet. Connect, please.", Toast.LENGTH_LONG).show()
                }
            }

            btnCafes.setOnClickListener {
                if (isInternetConnected) {
                    it.findNavController().navigate(R.id.action_homeFragment_to_cafesFragment)
                }else {
                    Toast.makeText((activity as MainActivity), "No Internet. Connect, please.", Toast.LENGTH_LONG).show()
                }
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.connectionLiveData = ConnectionLiveData((activity as MainActivity))

        connectionLiveData.observe((activity as MainActivity), Observer {connection ->
            if (connection!!.isConnected) {
                if (!isInternetConnected) {
                    Toast.makeText((activity as MainActivity), "Internet Connected!", Toast.LENGTH_SHORT).show()
                }

                isInternetConnected = true
            }else {
//                Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show()
                isInternetConnected = false
            }

        })
    }
}
