package com.chama.test.nearbyplaces.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chama.test.nearbyplaces.R
import com.chama.test.nearbyplaces.data.model.Place
import com.chama.test.nearbyplaces.databinding.ListItemPlaceBinding

class PlacesAdapter: RecyclerView.Adapter<PlacesAdapter.PlacesViewHolder>() {

    private val placesList = ArrayList<Place>()

    fun setList(places: List<Place>) {
        placesList.clear()
        placesList.addAll(places)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val binding: ListItemPlaceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item_place, parent, false)

        return PlacesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return placesList.size
    }

    override fun onBindViewHolder(holder: PlacesViewHolder, position: Int) {
        holder.bind(this.placesList[position])
    }

    inner class PlacesViewHolder(
        val binding: ListItemPlaceBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(place: Place) {
            binding.tvNamePlace.text = place.name
            binding.tvAddressPlace.text = place.vicinity
            if (place.rating != null) {
                binding.tvRatePlace.text = place.rating.toString()
            }else {
                binding.tvRatePlace.text = "(No Rate)"
            }
        }

    }

}