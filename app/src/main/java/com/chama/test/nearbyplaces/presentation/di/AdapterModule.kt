package com.chama.test.nearbyplaces.presentation.di

import com.chama.test.nearbyplaces.presentation.adapter.PlacesAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AdapterModule {

    @Singleton
    @Provides
    fun providePlacesAdapter(): PlacesAdapter {
        return PlacesAdapter()
    }
}
