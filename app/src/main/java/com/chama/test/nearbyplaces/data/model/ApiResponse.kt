package com.chama.test.nearbyplaces.data.model

import com.google.gson.annotations.SerializedName

data class ApiResponse(
    @SerializedName("html_attributions")
    val htmlAttributions: List<Any>,
    @SerializedName("results")
    val places: List<Place>,
    @SerializedName("status")
    val status: String
)