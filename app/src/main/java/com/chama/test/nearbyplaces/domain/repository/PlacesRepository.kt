package com.chama.test.nearbyplaces.domain.repository

import com.chama.test.nearbyplaces.data.model.Place

interface PlacesRepository {
    suspend fun getPlaces(location: String,
                          radius: String,
                          type: String): List<Place>?
}
