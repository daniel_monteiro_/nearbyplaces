package com.chama.test.nearbyplaces

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PlacesApp: Application()
