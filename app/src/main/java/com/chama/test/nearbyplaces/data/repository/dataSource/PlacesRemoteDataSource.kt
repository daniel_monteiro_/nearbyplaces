package com.chama.test.nearbyplaces.data.repository.dataSource

import com.chama.test.nearbyplaces.data.model.ApiResponse
import retrofit2.Response

interface PlacesRemoteDataSource {
    suspend fun getPlacesNear(location: String, radius: String, type: String): Response<ApiResponse>
}
