package com.chama.test.nearbyplaces.data.util

class ConnectionModel(val type: Int, val isConnected: Boolean)