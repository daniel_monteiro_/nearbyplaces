package com.chama.test.nearbyplaces.api

import com.chama.test.nearbyplaces.data.api.PlacesAPIService
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PlacesApiServiceTest {

    private lateinit var service: PlacesAPIService
    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder()
                .baseUrl(server.url(""))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PlacesAPIService::class.java)
    }

    private fun enqueueMockResponse(fileName: String) {
        val inputStream = javaClass.classLoader!!.getResourceAsStream(fileName)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun getPlacesNear_sentRequest_receivedExpcted() {
        runBlocking {
            enqueueMockResponse("placesresponse.json")
            val responseBody = service.getPlaces("-3.7858364,-38.4891727","500", "cafe").body()
            val request = server.takeRequest()
            Truth.assertThat(responseBody).isNotNull()
            Truth.assertThat(request.path).isEqualTo("/maps/api/place/nearbysearch/json?location=-3.7858364%2C-38.4891727&radius=500&type=cafe&key=AIzaSyDTkhqVctewIHQ6T4XgeBmz_rZfOj9hbXM")
        }
    }

    @Test
    fun getPlacesNear_receiveResponse_correctContent() {
        runBlocking {
            enqueueMockResponse("placesresponse.json")
            val responseBody = service.getPlaces("-3.7858364,-38.4891727","500", "cafe").body()
            val placesList = responseBody!!.places
            val place = placesList[0]

            Truth.assertThat(place.name).isEqualTo("Doce Escolha")
            Truth.assertThat(place.icon).isEqualTo("https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/cafe-71.png")
            Truth.assertThat(place.vicinity).isEqualTo("Rua Júnior Rocha, 233 - Luciano Cavalcante, Fortaleza")
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

}